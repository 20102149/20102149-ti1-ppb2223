class Cake {
  final int id;
  final String name;
  final String price;
  final String imageUrl;
  final bool isFavorite;
  final String subMenu;

  Cake({
    required this.id,
    required this.name,
    required this.price,
    required this.imageUrl,
    required this.isFavorite,
    required this.subMenu,
  });
}

final List<Cake> listCakes = [
  Cake(
    id: 1,
    name: 'Bolu marmer',
    price: '100.000',
    imageUrl: 'assets/Bolu marmer.jpg',
    isFavorite: true,
    subMenu: 'cake_box',
  ),
  Cake(
    id: 2,
    name: 'Bolu Gulung',
    price: '110.000',
    imageUrl: 'assets/bg.jpg',
    isFavorite: false,
    subMenu: 'cake_box',
  ),
  Cake(
    id: 5,
    name: 'Bolu tape',
    price: '60.000',
    imageUrl: 'assets/bg5.jpg',
    isFavorite: false,
    subMenu: 'cake_box',
  ),
  Cake(
    id: 6,
    name: 'Brownies Almond',
    price: '94.000',
    imageUrl: 'assets/bg6.jpg',
    isFavorite: false,
    subMenu: 'cake_box',
  ),
  Cake(
    id: 3,
    name: 'Chiffon Cheese',
    price: '30.000',
    imageUrl: 'assets/chiffon cheese.jpg',
    isFavorite: false,
    subMenu: 'cake_box',
  ),
  Cake(
    id: 4,
    name: 'Lapis Legit',
    price: '50.000',
    imageUrl: 'assets/Lapis Legit.jpg',
    isFavorite: true,
    subMenu: 'cake_box',
  ),
];