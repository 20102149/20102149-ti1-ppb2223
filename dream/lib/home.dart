import 'package:flutter/material.dart';
import 'package:dream/screen/cakery_page.dart';
import 'package:dream/widget/navbar_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 198, 126, 31),
        elevation: 0.0,
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Color.fromARGB(255, 198, 126, 31),
          ),
          onPressed: () {},
        ),
        title: Text(
          'Dream Bakery',
          style: TextStyle(
            fontFamily: 'Varela',
            fontSize: 24.0.sp,
            color: Color.fromARGB(255, 0, 0, 0),
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.notifications_none,
              color: Color.fromARGB(255, 0, 0, 0),
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        children: [
          SizedBox(height: 8.0.h),
          Text('Menu',
              style: TextStyle(
                  fontFamily: 'Varela',
                  fontSize: 40.0.sp,
                  fontWeight: FontWeight.bold)),
          SizedBox(height: 8.0.h),
          TabBar(
              controller: _tabController,
              indicatorColor: Color.fromARGB(255, 198, 126, 31),
              labelColor: Color.fromARGB(255, 0, 0, 0),
              isScrollable: true,
              labelPadding: const EdgeInsets.only(right: 10),
              unselectedLabelColor: Color.fromARGB(255, 154, 149, 149),
              tabs: [
                Tab(
                  child: Text('Kue Kering',
                      style: TextStyle(
                        fontFamily: 'Varela',
                        fontSize: 20.0.sp,
                      )),
                ),
                Tab(
                  child: Text('Kue Basah',
                      style: TextStyle(
                        fontFamily: 'Varela',
                        fontSize: 20.0.sp,
                      )),
                ),
              ]),
          SizedBox(
            height: MediaQuery.of(context).size.height - 100.0.h,
            width: double.infinity,
            child: TabBarView(
              controller: _tabController,
              children: const [
                CakeryPage(),
                CakeryPage(),
                CakeryPage(),
              ],
            ),
          )
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: const Color(0xFFF17532),
        child: const Icon(Icons.cake),
      ),
      bottomNavigationBar: const NavbarWidget(),
    );
  }
}